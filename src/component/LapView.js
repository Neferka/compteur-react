import React from 'react';
const LapView = (props) => {
    const {laps}= props

    function formatTime(value){
        let seconds= value %60;
        let minutes= ((value-seconds)/60)%60;
        let hours= ((value-value%3600)/3600);
        return `${hours} : ${minutes} : ${seconds}  `
    }

    return (
        <div>
            {laps.map(lap => {return <p key={lap} > {formatTime(lap)}</p>})}
        </div>
    );
}

export default LapView;
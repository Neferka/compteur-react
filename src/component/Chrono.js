import React from 'react';

const Chrono = (props) => {
    const {currentime}=props

    function formatTime(){
        let seconds= currentime %60;
        if (seconds< 10){
            seconds= '0'+seconds
        }
        let minutes= ((currentime-seconds)/60)%60;
        if (minutes< 10){
            minutes= '0'+minutes
        }
        let hours= ((currentime-currentime%3600)/3600);
        if (hours< 10){
            hours= '0'+hours
        }
        return `${hours} : ${minutes} : ${seconds}  `
    }

    return (
    <p> {formatTime()}</p>
    );
}

export default Chrono;
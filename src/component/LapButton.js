import React from 'react';

const LapButton = (props) => {
    const {lap}=props
    const{isRunning}=props

    return (
        <button onClick= {lap} type="button" disabled={!isRunning}>Lap</button>
    );
}

export default LapButton;
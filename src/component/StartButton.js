import React from 'react';
const StartButton = (props) => {

    const {startChrono}=props
    const{isRunning}=props

    return (
        <button onClick= {startChrono} type="button" disabled={isRunning} >Start</button>
    );
}

export default StartButton;
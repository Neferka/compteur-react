import React from 'react';

const ResetButton = (props) => {
    const {resetChrono}=props
    const{isRunning}=props
    return (
        <button onClick= {resetChrono} type="button">Reset</button>
    );
}

export default ResetButton;
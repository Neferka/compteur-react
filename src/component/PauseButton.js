import React from 'react';

const PauseButton = (props) => {
    const {pauseChrono}=props
    const{isRunning}=props

    return (
        <button onClick= {pauseChrono} type="button"disabled={!isRunning} >Pause</button>
    );
}

export default PauseButton;
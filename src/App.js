import React, { useState,useEffect, useRef } from 'react';
import Chrono from "./component/Chrono"
import './App.css';
import StartButton from './component/StartButton';
import PauseButton from './component/PauseButton';
import ResetButton from './component/ResetButton';
import LapButton from './component/LapButton';
import LapView from './component/LapView';

function App() {

  const chronoTime = useRef(null);

  const [isRunning, setIsRunning]= useState(false)
  const[currentime, setCurrentTime]= useState(0)
  const [laps,setLaps]=useState([])


function startChrono(){
chronoTime.current = setInterval(() => {
  setCurrentTime(currentime => currentime+1)
}, 10);
setIsRunning(true)
}

function pauseChrono(){
clearInterval(chronoTime.current);
setIsRunning(false)
}

function resetChrono(){
setCurrentTime(0)
}

function lap(){
let newLaps= laps;
newLaps.push(currentime)
setLaps(newLaps)
}
  return (
 <div>
<Chrono currentime= {currentime}/>
<LapView laps={laps}/>
<StartButton startChrono= {startChrono} isRunning={isRunning}/>
{!isRunning && currentime>0 ? <ResetButton resetChrono={resetChrono} isRunning={isRunning}/> : <PauseButton pauseChrono= {pauseChrono} isRunning={isRunning}/>}

<LapButton lap={lap} isRunning={isRunning}/>

 </div>
  );
}

export default App;
